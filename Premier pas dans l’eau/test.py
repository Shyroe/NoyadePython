import unittest
from function import *


class MyTestCase(unittest.TestCase):
    def test_day_of_week(self):
        result = day_of_week("1/8/1947")
        self.assertEqual("Vendredi", result)

    def test_switch_day_of_week_ok(self):
        result = switch(4, "day_of_week")
        self.assertEqual("Vendredi", result)

    def test_switch_day_of_week_invalid(self):
        result = switch(7, "day_of_week")
        self.assertEqual("Invalid day", result)

    def test_switch_month_ok(self):
        result = switch(8, "month")
        self.assertEqual(2, result)

    def test_switch_month_invalid(self):
        result = switch(0, "month")
        self.assertEqual("Invalid month", result)

    def test_switch_years_ok(self):
        result = switch(19, "years")
        self.assertEqual(0, result)

    def test_switch_years_invalid(self):
        result = switch(0, "years")
        self.assertEqual("Invalid years", result)

    def test_switch_day_ok(self):
        result = switch(5, "day")
        self.assertEqual("Vendredi", result)

    def test_switch_day_invalid(self):
        result = switch(7, "day")
        self.assertEqual("Invalid day", result)

    def test_bissextile_true(self):
        result = bissextile(2020)
        self.assertEqual(True, result)

    def test_bissextile_false(self):
        result = bissextile(1947)
        self.assertEqual(False, result)

    def test_calcul_jour_semaine(self):
        result = calcul_jour_semaine("1/8/1947")
        self.assertEqual("Vendredi", result)


if __name__ == '__main__':
    unittest.main()
