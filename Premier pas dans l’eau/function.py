from datetime import datetime


# Return jour de la semaine
# Params : string date_string : une date sous la forme jour/mois/annee
def day_of_week(date_string):
    format_str = '%d/%m/%Y'
    datetime_obj = datetime.strptime(date_string, format_str)
    date_day = switch(datetime_obj.date().weekday(), "day_of_week")
    return date_day


# Return jour de la semaine
# Params : int argument : key d'un dictionnaire
#          string name_switch : nom du switch dictionnaire pour les differencier
def switch(argument, name_switch):
    if name_switch == "day_of_week":
        # Correspondance entre un chiffre et le jour pour la methode 1
        switcher_day_of_week = {
            0: "Lundi",
            1: "Mardi",
            2: "Mercredi",
            3: "Jeudi",
            4: "Vendredi",
            5: "Samedi",
            6: "Dimanche"
        }
        return switcher_day_of_week.get(int(argument), "Invalid day")

    elif name_switch == "month":
        #  Correspondance entre un mois et le chiffre a ajouter pour les calculs
        switcher_month = {
            1: 0,
            2: 3,
            3: 3,
            4: 6,
            5: 1,
            6: 4,
            7: 6,
            8: 2,
            9: 5,
            10: 0,
            11: 3,
            12: 5
        }
        return switcher_month.get(int(argument), "Invalid month")

    elif name_switch == "years":
        #  Correspondance entre une annee et le chiffre a ajouter pour les calculs
        switcher_years = {
            16: 6,
            17: 4,
            18: 2,
            19: 0,
            20: 6,
            21: 4
        }
        return switcher_years.get(int(argument), "Invalid years")

    elif name_switch == "day":
        # Correspondance entre un chiffre et le jour pour la methode 2
        switcher_day = {
            0: "Dimanche",
            1: "Lundi",
            2: "Mardi",
            3: "Mercredi",
            4: "Jeudi",
            5: "Vendredi",
            6: "Samedi",
        }
        return switcher_day.get(int(argument), "Invalid day")


# Return si une annee est bissextile(true) ou non (false)
# Params : int annee : annee a tester
def bissextile(annee):
    # Si l'annee n'est divisible par 4
    if annee % 4 != 0:
        return False
    # Si l'annee est divisible par 100
    # Et si l'annee n'est divisible par 400
    if annee % 100 == 0 and annee % 400 != 0:
        return False
    return True


# Return jour de la semaine
# Params : string date_string : une date sous la forme jour/mois/annee
def calcul_jour_semaine(date_string):
    # Decoupage de la date en differente variable jour, mois, annee
    date_split = date_string.split("/")
    jour = date_split[0]
    mois = date_split[1]
    annee = date_split[2]

    # Decoupage de l'annee
    # start_number_annee : les 2 premiers chiffres
    # last_number_annee : les 2 derniers chiffres
    start_number_annee = list(annee)[0] + list(annee)[1]
    last_number_annee = list(annee)[2] + list(annee)[3]

    # Ajout 1/4 des 2 derniers chiffres de l'annee en ignorant le reste
    ajout_1_4 = int(last_number_annee) + int(last_number_annee) // 4

    # Ajout du jour au resultat precedent
    ajout_jour = ajout_1_4 + int(jour)

    # En fonction du mois, ajout d'un certain chiffre au resultat precedent
    ajout_mois = ajout_jour + switch(mois, "month")

    # Si l'année est bissextile et le mois est janvier ou février, ote 1 du resultat precedent
    if bissextile(int(annee)) and (int(mois) == 1 or int(mois) == 2):
        ajout_mois = ajout_mois - 1

    # En fonction de l'anne, ajout d'un certain chiffre au resultat precedent
    ajout_annee = ajout_mois + switch(start_number_annee, "years")

    # Reste de la division par 7 du resultat des calculs precedent
    reste_division_7 = ajout_annee % 7

    # Correspondance entre le chiffre qui determine le jour et le nom de ce jour
    result = switch(int(reste_division_7), "day")
    return result