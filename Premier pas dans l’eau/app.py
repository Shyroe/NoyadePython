from function import *

date_str = input("Saisir une date (sous la forme : jour/mois/annee) : ")

# Methode 1 : utilisation de la fonction python weekday()
print("\nMethode 1 : (utilisation weekday())")
try:
    day_methode1 = day_of_week(date_str)
except:
    print("Format de la date invalide")
else:
    print("Le " + date_str + " est un " + day_methode1)

# Methode 2 : utilisation de mes propres fonction
print("\nMethode 2 : (utilisation fonction perso)")
try:
    day_methode2 = calcul_jour_semaine(date_str)
except:
    print("Format de la date invalide")
else:
    print("Le " + date_str + " est un " + day_methode2)
